extern crate rand;

use std::io;
use self::rand::Rng;
use std::cmp::Ordering;

//mod ugadaka;
//ugadaka::start();
pub fn start() {
    println!("Угадайте число!\n");
    let secret  = rand::thread_rng().gen_range(1,101);
    loop {

        //println!("Загаданное число = {}" , secret);

        println!("Пожалуйста введите предположение:");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess)
            .ok()
            .expect("Не удалось прочитать строку:(");

            println!("Ваша попытка: {}", guess);

            let guess_int: u32 = match guess.trim().parse(){
                Ok(num) => num,
                Err(_) => {
                    println!("Это не число, чувак!");
                    continue;
                },
            };

            match guess_int.cmp(&secret) {
                Ordering::Less       => println!("Слишком маленькое!"),
                Ordering::Greater    => println!("Слишком большое!"),
                Ordering::Equal      => {
                    println!("Вы выиграли!");
                    break;
                }
            }
    }
}
