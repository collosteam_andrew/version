extern crate argparse;
extern crate ansi_term;

use argparse::{ArgumentParser, Store};
use ansi_term::Colour::{Red, Blue , Green};
use std::fs::File;
use std::io::{Read,Write};

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main(){
    let DEBUG = false;
    let message: &str = "Android build version incrementor v.";
    let result = format!("{}{}",message , VERSION).to_string();
    let mut gradle_file_path = "".to_string();
    let mut build_number_pattern = "BUILD_NUMBER".to_string();
    let mut build_number = "".to_string();

    let cur_dir = std::env::current_dir().unwrap();

    if(DEBUG){
    println!("Current dir is > {}",  cur_dir.display());
    }

    let cur_dir_str = format!("{}",cur_dir.display()).to_string();

    {
        let mut ap = ArgumentParser::new();
        ap.set_description(&result);
        ap.refer(&mut gradle_file_path)
                    .add_option(&["-g","--gradle_file_path"],
                            Store ,
                            "Path to application gradle.build file");
        ap.refer(&mut build_number_pattern)
                    .add_option(&["-p","--number_pattern"],
                            Store ,
                            "Build number text pattren, for default - BUILD_NUMBER");
        ap.refer(&mut build_number)
                    .add_option(&["-n","--build_number"],
                            Store ,
                            "Use this arg for set actual build number");
        ap.parse_args_or_exit();
    }



    if gradle_file_path.trim().len() == 0{
        println!("{}",Red.bold().paint("ERROR! Path to gradle file can't be empty!"));
        return;
    }

    if build_number.trim().len() == 0{
        println!("{}",Red.bold().paint("ERROR! Build number can't be empty!"));
        return;
    }

    let path_to_build_gradle = format!("{}{}",cur_dir_str , gradle_file_path.clone()).to_string();

    if DEBUG {
        println!("Gradle path > {}{}" , cur_dir_str , gradle_file_path);
        println!("Build version pattern > {}" , build_number_pattern);

    }

    let path_to_gradle_copy = path_to_build_gradle.clone();
    let mut file = match File::open(path_to_build_gradle) {
        Ok(file) => file,
        Err(..)  => panic!("File not found error!"),
    };

    let mut buffer = String::new();

    file.read_to_string(&mut buffer);

    drop(file);

    let mut data = format!("{}",buffer);

    if DEBUG {
         let mut debug_data = data.clone();
         println!("{}", Blue.bold().paint("\n File > Origin > \n"));
         println!("{}", Green.bold().paint(debug_data));
    }

    let with_buil_number = data.contains(&build_number_pattern);

        if !with_buil_number  {
           println!("{}",Red.bold().paint("ERROR! Build number pattern not found"));
           if(!DEBUG){
                panic!("Build number pattern not found")
           }
           return;
        }

        let data_result = data.replace(&build_number_pattern , &build_number);

        if DEBUG {
            println!("{}", Blue.bold().paint("\n File > Result > \n"));
            println!("{}", Green.bold().paint(data_result.clone()));
        }



        let mut file_result = match File::create(path_to_gradle_copy) {
            Ok(file) => file,
            Err(..)  => panic!("File not created error!"),
        };

        file_result.write_all(data_result.into_bytes().as_ref());

        drop(file_result);
        println!("{}", Blue.bold().paint(format!("New build number > {}" ,build_number).to_string()));

}
