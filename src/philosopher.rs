use std::thread;
use std::sync::{Mutex, Arc};
use std::time::Duration;

struct Table {
    forks: Vec<Mutex<()>>,
}

struct Philosopher {
    name: String,
    left: usize,
    right: usize,
}

impl Philosopher {
    fn new(name: &str , left: usize , right: usize) -> Philosopher{
        Philosopher{
         name: name.to_string(),
         left: left,
         right: right,
        }
    }

    fn eat(&self , table: &Table){
        let left_ = table.forks[self.left].lock().unwrap();
        let right_ = table.forks[self.right].lock().unwrap();

        // println!("Left - {}", left_);
        // println!("Right - {}", right_);

        println!("{} начала есть!", self.name);

        thread::sleep(Duration::from_millis(1000));

        println!("{} закончила есть!", self.name);
    }
}

fn mein_method(){

    let table = Arc::new(Table{forks: vec![
            Mutex::new(()),
            Mutex::new(()),
            Mutex::new(()),
            Mutex::new(()),
            Mutex::new(()),
        ]});

    let philosophers = vec![
            Philosopher::new("Джудит Батлер", 0, 1),
            Philosopher::new("Рая Дунаевская", 1, 2),
            Philosopher::new("Наталья Зарубина", 2, 3),
            Philosopher::new("Эма Гольдман", 3, 4),
            Philosopher::new("Анна Шмидт", 0, 4),
    ];

    let handles: Vec<_> = philosophers.into_iter().map(|p|{
        let table = table.clone();

        thread::spawn(move || {
            p.eat(&table);
        })
    }).collect();

    for h in handles {
        h.join().unwrap();
    }

}
